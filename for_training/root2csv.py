import ROOT
import time

from root_numpy import tree2array, array2root, tree2rec

import sys
import os

debug = 0


def do_root2csv(rootfilepath='', treename='CollectionTree', csvfilename='', flag_ttv=1, feature_line='', flag_fold=0, flag_nominal=0):
    t0 = time.clock()
    if feature_line == '':
        feature_line='EventNumber,Weight,Weight_BTagSF85,ThirdLeptonPt,SubleadingInvMass,LeptonicWMass,DimuonPt,NCentralJets,NJets_WP85,CorrectFourthLeptonPt,FourthThirdInvMass,CorrectLeptonicTopMass,HT,CosThetaStar,FirstBreitLikelihoodTopCandMass'
    feature_list = feature_line.split(',')
    if flag_nominal==1:  ## Var = 17
        feature_nominal_line='EventNumber,Weight,Weight_BTagSF85,FirstBreitLikelihoodWInvMass,FirstBreitLikelihoodTopCandMass,SubleadingInvMass,LeptonicWMass,CorrectLeptonicTopMass,ThirdLeptonPt,NJets,NCentralJets,DimuonPt,NJets_WP85,CosThetaStar,LeadingJetPt,SubleadingJetPt,MissingET,HT,CorrectFourthLeptonPt,FourthThirdInvMass'
        feature_line='EventNumber,Weight,Weight_BTagSF85,FirstBreitLikelihoodWInvMass,FirstBreitLikelihoodTopCandMass,SubleadingInvMass,LeptonicWMass,CorrectLeptonicTopMass,ThirdLeptonPt,NJets,NCentralJets,DimuonPt,NJets_WP85,CosThetaStar,LeadingJetPt,SubleadingJetPt,MissingET,HT,CorrectFourthLeptonPt,FourthThirdInvMass'
        feature_list = feature_nominal_line.split(',')
    n_feature = len(feature_list)

    in_file = ROOT.TFile(rootfilepath, "read")
    tree = in_file.Get(treename)
    n_features = len(feature_list)
    branches = feature_list
    print("before all_events Time = " + str((time.clock()-t0)/60.) + " minutes")
    all_events = tree2array(
            tree,
            branches=branches
            )
    n_events = len(all_events)
    print("after all_events Time = " + str((time.clock()-t0)/60.) + " minutes")
    print("n_events =",n_events)
    n_show = 5
    if n_show > n_events:
        n_show = n_events
    for i in range(n_show):
        if debug:
            print("all_events[",i,"] =",all_events[i])
    if debug and n_events > 10:
        n_events = 10
        print("DEBUG n_events =",n_events)
    print("before Time = " + str((time.clock()-t0)/60.) + " minutes")
    
    list_features = []
    list_others = []
    list_train_features = []
    list_train_weight0 = []
    list_train_others= []
    list_valid_features = []
    list_valid_weight0 = []
    list_valid_others= []
    list_test_features = []
    list_test_weight0 = []
    list_test_others= []

    list_all = []
    list_train = []
    list_test = []
    list_valid = []
    
    #with open(csvfilename+'_features.all', "w") as fall:
    for i in range(n_events):
        aevent = all_events[i]
        eventNumber = int(aevent[0])
        line = ''
        for j in range(n_feature):
            if j == n_feature-1:
                line += str(float(aevent[j]))
            elif j == 0:
                line += str(aevent[j])+','
            else:
                line += str(float(aevent[j]))+','
                pass
            pass
        list_all.append(line)
        if flag_fold == 1:
            if eventNumber%4 == 1 or eventNumber%4 == 2:
                list_train .append(line)
            elif eventNumber%4 == 0:
                list_test.append(line)
            elif eventNumber%4 == 3:
                list_valid.append(line)
        elif flag_fold == 2:
            if eventNumber%4 == 2 or eventNumber%4 == 3:
                list_train .append(line)
            elif eventNumber%4 == 1:
                list_test.append(line)
            elif eventNumber%4 == 0:
                list_valid.append(line)
        elif flag_fold == 3:
            if eventNumber%4 == 0 or eventNumber%4 == 3:
                list_train .append(line)
            elif eventNumber%4 == 2:
                list_test.append(line)
            elif eventNumber%4 == 1:
                list_valid.append(line)
        elif flag_fold == 4:
            if eventNumber%4 == 1 or eventNumber%4 == 0:
                list_train .append(line)
            elif eventNumber%4 == 3:
                list_test.append(line)
            elif eventNumber%4 == 2:
                list_valid.append(line)
    print('fill train/test/valid things...')
    with open(csvfilename+'.csv', "w") as f_all:
        f_all.write(feature_line+'\n')
        for line in list_all:
            f_all.write(line + '\n')
        pass
    print("zihang")
    if flag_ttv != 1:
        print("after Time = " + str((time.clock()-t0)/60.) + " minutes")
        return
    with open(csvfilename+'_train.csv', "w") as f_train:
        f_train.write(feature_line+'\n')
        for line in list_train:
            f_train.write(line + '\n')
        pass
    with open(csvfilename+'_test.csv', "w") as f_test:
        f_test.write(feature_line+'\n')
        for line in list_test:
            f_test.write(line + '\n')
        pass
    with open(csvfilename+'_val.csv', "w") as f_valid:
        f_valid.write(feature_line+'\n')
        for line in list_valid:
            f_valid.write(line + '\n')
        pass

    print("after Time = " + str((time.clock()-t0)/60.) + " minutes")

rootfilepath = '/eos/home-z/zijia/HHyybb/Ntuple/HHbbyy_cHHH01d0.root'
treename = 'skimTree'
csvfilename = 'HHbbyy_cHHH01d0'
feature_line = ''
flag_ttv = 1
flag_fold = 0
flag_nominal = 0
if len(sys.argv)<3:
    print("The argments are rootfilepath, treename, csvfilename.")
    print("The argments are rootfilepath, treename, csvfilename.")
    print("The argments are rootfilepath, treename, csvfilename.")
if len(sys.argv)>3:
    rootfilepath = sys.argv[1]
    treename = sys.argv[2]
    csvfilename = sys.argv[3]
if len(sys.argv)>4:
    flag_ttv = int(sys.argv[4])
if len(sys.argv)>5:
    flag_fold = int(sys.argv[5])
if len(sys.argv)>6:
    flag_nominal = int(sys.argv[6])
if len(sys.argv)>3 :
    do_root2csv(rootfilepath,treename,csvfilename, flag_ttv,feature_line, flag_fold, flag_nominal)
