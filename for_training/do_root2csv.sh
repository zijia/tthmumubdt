ntupledir=/eos/home-z/zijia/Hmumu/tthmumustudies/mini_trees

#csvdir=.
csvdir=/eos/home-z/zijia/Hmumu/tthmumustudies/mini_csv

samples="344388 410472 410218 410219 410220 410156 410157 363356 363358 364250 364253 364254 410550 346343 346344 346345 410155"

for ifold in 1 2 3 4;do
mkdir ${csvdir}/fold_${ifold}
for y in mc16a mc16d mc16e;do
for sample in ${samples}
do
    ntuplepath=${ntupledir}/${y}.${sample}.root
    csvpath=${csvdir}/fold_${ifold}/${y}_${sample}
    flag_ttv=1
    flag_fold=${ifold}
    flag_nominal=0
    if [[ ${sample} == 'data' ]]; then
        flag_ttv=1
    fi
    echo "working on fold ${ifold}, ${y}.${sample} ..."
    if [[ ! -f ${ntuplepath} ]]; then
        echo "${ntuplepath} does not exists!"
        continue
    fi
    if [[ -f ${csvpath}.csv ]]; then
        #echo "${csvpath}.csv already exists!"
        if [[ -f ${csvpath}_train.csv ]]; then
            if [[ -f ${csvpath}_test.csv ]]; then
                if [[ -f ${csvpath}_val.csv ]]; then
                    continue
                fi
            fi
        fi
    elif [[ -f /eos/home-z/zijia/Hmumu/tthmumustudies/mini_csv/fold_${ifold}/${y}_${sample}.csv ]]; then
        if [[ -f /eos/home-z/zijia/Hmumu/tthmumustudies/mini_csv/fold_${ifold}/${y}_${sample}_train.csv ]]; then
            if [[ -f /eos/home-z/zijia/Hmumu/tthmumustudies/mini_csv/fold_${ifold}/${y}_${sample}_test.csv ]]; then
                if [[ -f /eos/home-z/zijia/Hmumu/tthmumustudies/mini_csv/fold_${ifold}/${y}_${sample}_val.csv ]]; then
        #echo "/eos/home-z/zijia/Hmumu/tthmumustudies/mini_csv/fold_${ifold}/${y}_${sample}.csv already exists!"
                    continue
                fi
            fi
        fi
    fi

    echo "running root2csv.py"
    nohup python root2csv.py ${ntuplepath} TrainingTree ${csvpath} ${flag_ttv} ${flag_fold} ${flag_nominal} &

done
done
done
