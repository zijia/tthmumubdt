#version=LCG_96bpython3
version=LCG_94python3
#setupATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
#asetup AnalysisBase,21.2.131
export PYTHONPATH=$PYTHONPATH:$PWD
#lsetup "lcgenv -p ${version} x86_64-centos7-gcc8-opt tensorflow"
#lsetup "lcgenv -p ${version} x86_64-centos7-gcc8-opt ROOT"
lsetup "lcgenv -p ${version} x86_64-centos7-gcc8-opt numpy"
#lsetup "lcgenv -p ${version} x86_64-centos7-gcc8-opt pandas"
#lsetup "lcgenv -p ${version} x86_64-centos7-gcc8-opt xgboost"
lsetup "lcgenv -p ${version} x86_64-centos7-gcc8-opt root_numpy"
#lsetup "lcgenv -p ${version} x86_64-centos7-gcc8-opt networkx"
#lsetup "lcgenv -p ${version} x86_64-centos7-gcc8-opt scikitlearn"
#lsetup "lcgenv -p ${version} x86_64-centos7-gcc8-opt joblib"
#lsetup "lcgenv -p ${version} x86_64-centos7-gcc8-opt PyYAML"
#lsetup "lcgenv -p ${version} x86_64-centos7-gcc8-opt matplotlib"
#lsetup "lcgenv -p ${version} x86_64-centos7-gcc8-opt lightgbm"
#lsetup "lcgenv -p ${version} x86_64-centos7-gcc8-opt pytables"
ulimit -S -s unlimited
