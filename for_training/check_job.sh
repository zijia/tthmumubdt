ntupledir=/eos/home-z/zijia/Hmumu/tthmumustudies/mini_trees

csvdir=.
#csvdir=/eos/home-z/zijia/Hmumu/tthmumustudies/mini_csv

samples="344388 410472 410218 410219 410220 410156 410157 363356 363358 364250 364253 364254 410550 346343 346344 346345 410155"

for y in mc16a mc16d mc16e;do
for sample in ${samples}
do
    csvpath=${csvdir}/fold_*/${y}_${sample}
    count_eos=`ls /eos/home-z/zijia/Hmumu/tthmumustudies/mini_csv/fold_*/${y}_${sample}*.csv | wc -l`
    count_afs=`ls ${csvpath}.csv | wc -l`
    echo $count_eos
    if [[ $count_eos -eq 16 ]]; then
        echo "eos ${y}_${sample}" >> log
        continue
    #elif [[ $count_afs -eq 16 ]]; then
    #    #mv ${csvdir}/fold_*/${y}_${sample}*csv /eos/home-z/zijia/Hmumu/tthmumustudies/mini_csv/fold_*/
    #    continue
    else
        echo "${y}.${sample} missing" >> log
    fi
done
done
