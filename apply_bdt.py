#import sys
#sys.path.append("../XGB-Opt/root_pandas")
import os
#from root_pandas import read_root
import pandas as pd
import numpy as np
import ROOT
#import uproot
import xgboost as xgb
import matplotlib.pyplot as plt
import argparse
from sklearn.preprocessing import QuantileTransformer
import pickle

trainVar=["ThirdLeptonPt", "SubleadingInvMass", "LeptonicWMass", "DimuonPt", "NCentralJets", "NJets_WP85", "CorrectFourthLeptonPt", "FourthThirdInvMass", "CorrectLeptonicTopMass", "HT", "CosThetaStar", "FirstBreitLikelihoodTopCandMass"]

parser = argparse.ArgumentParser()
parser.add_argument("-s","--sampleName",type=str,default="data")
parser.add_argument("-m","--modelFolder",type=str,default="model_1")
parser.add_argument("-v","--var",type=str,default="Var12")
parser.add_argument("-o","--outFolder",type=str,default="ntuples")

args = parser.parse_args()
os.system("mkdir -vp %s/%s"%(args.outFolder, args.var))

do = True
## ========================================================================
## generate Quantile transformation pickle file 
## ========================================================================
qt_file = os.path.join(args.modelFolder, args.var, "transformer_"+args.sampleName+".pickle")
if do==True:
    print("== generate QT pickle ==")
    dVal={}
    quantile_transform = {}
    for fold in [1,2,3,4]:
        print("this is ", fold)
        dFTest=pd.read_csv("/eos/home-z/zijia/Hmumu/tthmumustudies/mini_csv/fold_%d/%s_val.csv"%(fold,args.sampleName))
        #dFTest=pd.concat(dFTest,ignore_index=True)
        dVal[fold]=xgb.DMatrix(dFTest[trainVar],feature_names=trainVar)
        print(dVal[fold])

        Model=xgb.Booster()
        Model.load_model("%s/%s/fold_%d.model"%(args.modelFolder, args.var, fold))

        raw_scores=Model.predict(dVal[fold])
        print(raw_scores)

        quantile_transform[fold] = QuantileTransformer()#n_quantiles=10000, subsample=1000000)
        quantile_transform[fold].fit(raw_scores.reshape(-1, 1))
    with open(qt_file, "wb") as f: pickle.dump(quantile_transform,f)
    print("== generated QT pickle as %s =="%(qt_file))

## ========================================================================
## apply scores and QT 
## ========================================================================
print("hello")
with open(qt_file, 'rb') as f: transformers = pickle.load(f)
dVal={}
for fold in [1,2,3,4]:
    print("this is ", fold)
    dFTest=pd.read_csv("/eos/home-z/zijia/Hmumu/tthmumustudies/mini_csv/fold_%d/%s_val.csv"%(fold,args.sampleName))
    #dFTest=pd.concat(dFTest,ignore_index=True)
    dVal[fold]=xgb.DMatrix(dFTest[trainVar],feature_names=trainVar)

    Model=xgb.Booster()
    Model.load_model("%s/%s/fold_%d.model"%(args.modelFolder, args.var, fold))

    raw_scores=Model.predict(dVal[fold])
    print(raw_scores.reshape(-1,1))
    print(transformers[fold])
    transformed_scores = transformers[fold].transform(raw_scores.reshape(-1,1))
    print("hello")
    scores = transformed_scores.reshape(1,-1)[0]
    print(scores)
    #fig,ax=plt.subplots()
    #plt.hist(np.array(scores), bins=25, color='cornflowerblue',label="QT score")
    #fig.tight_layout()
    #fig.savefig("%s/%s/fold_%d_QT.pdf"%(args.modelFolder, args.var, fold))
