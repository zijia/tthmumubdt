#import sys
#sys.path.append("../XGB-Opt/root_pandas")
import os
#from root_pandas import read_root
import pandas as pd
import numpy as np
import ROOT
#import uproot
import xgboost as xgb
import matplotlib.pyplot as plt
import argparse
trainVar=["ThirdLeptonPt", "SubleadingInvMass", "LeptonicWMass", "DimuonPt", "NCentralJets", "NJets_WP85", "CorrectFourthLeptonPt", "FourthThirdInvMass", "CorrectLeptonicTopMass", "HT", "CosThetaStar", "FirstBreitLikelihoodTopCandMass"]

parser = argparse.ArgumentParser()
parser.add_argument("-o","--outFolder",type=str,default="model_1")
parser.add_argument("-v","--var",type=str,default="Var12")
parser.add_argument("-f","--fold",type=int,default=1)

args = parser.parse_args()

params={"objective":"binary:logistic","eval_metric": ["logloss"],
        "tree_method": "hist",
        "booster": "gbtree",
        "grow_policy": "lossguide",
        "min_child_weight": 186,
        #"colsample_bytree": 1, #0.9819092383894799, #1.0, #0.8673434869122754,#0.9819092383894799,
        #"scale_pos_weight": 1.8211515942341352, #0.5334454667979189, #2.823702271035043, #5.556800607135357, #0.5334454667979189,
        #"max_delta_step": 10.439085145323304, #5.048379451060231, #20.0, #15.622420578238948, #5.048379451060231,
        "subsample": 1.0, #0.5773621365665756, #1.0, #0.6548383460703933, #0.5773621365665756,
        "eta": 0.05, #0.016850511062762245, #0.4551736635815177, #0.01, #0.016850511062762245,
        "alpha": 0., #0.5590710084256435,# 0.0, #0.5590710084256435,
        "lambda": 0.5, #4.272114317747015, #10, #0.20651287762542891, #4.272114317747015,
        "max_depth": 4, #3, #11, #14,
        "gamma": 0., #1.482708463407503, #0, #1.482708463407503,
        #"max_bin": 256, #152# 500, #429 #152
        }

os.system("mkdir -vp %s/%s"%(args.outFolder, args.var))

#fileList=["mc16a_344388", "mc16d_346343"]
fileList=["mc16a_344388", "mc16d_344388", "mc16e_344388", "mc16a_410472", "mc16a_410218", "mc16a_410219", "mc16a_410220", "mc16a_410156", "mc16a_410157", "mc16a_363356", "mc16a_363358", "mc16a_364250", "mc16a_364253", "mc16a_364254", "mc16a_410550", "mc16a_346343", "mc16a_346344", "mc16a_346345", "mc16a_410155", "mc16d_410472", "mc16d_410218", "mc16d_410219", "mc16d_410220", "mc16d_410156", "mc16d_410157", "mc16d_363356", "mc16d_363358", "mc16d_364250", "mc16d_364253", "mc16d_364254", "mc16d_410550", "mc16d_346343", "mc16d_346344", "mc16d_346345", "mc16d_410155", "mc16e_410472", "mc16e_410218", "mc16e_410219", "mc16e_410220", "mc16e_410156", "mc16e_410157", "mc16e_363356", "mc16e_363358", "mc16e_364250", "mc16e_364253", "mc16e_364254", "mc16e_410550", "mc16e_346343", "mc16e_346344", "mc16e_346345", "mc16e_410155"]
dTrain={}
dVal={}
aucValue = 0
loglossR = 0
for fold in [args.fold]:
#for fold in [1,2,3,4]:
    dFTrainList=[]
    dFTestList=[]
    sum_signal_train=0
    sum_signal_test =0
    sum_bkg_train =0
    sum_bkg_test  =0
    for i,fileName in enumerate(fileList):
        dFTrain=pd.read_csv("/eos/home-z/zijia/Hmumu/tthmumustudies/mini_csv/fold_%d/%s_train.csv"%(fold,fileName))
        #dFTrain=dFTrain_raw[dFTrain_raw["Weight"]*dFTrain_raw["Weight_BTagSF85"]>0] #Weight_BTagSF85, EventNumber
        dFTest=pd.read_csv("/eos/home-z/zijia/Hmumu/tthmumustudies/mini_csv/fold_%d/%s_val.csv"%(fold,fileName))
        #dFTest=dFTest_raw[dFTest_raw["Weight"]*dFTest_raw["Weight_BTagSF85"]>0]
        #print("start filliing signal and bkg 1st")
        if i<3:
            dFTrain["WeightProd"]=dFTrain["Weight"]*dFTrain["Weight_BTagSF85"]
            dFTest["WeightProd"]=dFTest["Weight"]*dFTest["Weight_BTagSF85"]
            sum_signal_train+=np.sum(dFTrain["WeightProd"])
            sum_signal_test +=np.sum(dFTest["WeightProd"])
        else:
            #print(fileName)
            dFTrain["WeightProd"]=dFTrain["Weight"]*dFTrain["Weight_BTagSF85"]
            dFTest["WeightProd"]=dFTest["Weight"]*dFTest["Weight_BTagSF85"]
            sum_bkg_train+=np.sum(dFTrain["WeightProd"])
            sum_bkg_test +=np.sum(dFTest["WeightProd"])
    print("zihang sum of signal: ",sum_signal_train, sum_signal_test)
    print("zihang sum of bkg: ",sum_bkg_train, sum_bkg_test)
    for i,fileName in enumerate(fileList):
        dFTrain=pd.read_csv("/eos/home-z/zijia/Hmumu/tthmumustudies/mini_csv/fold_%d/%s_train.csv"%(fold,fileName))
        dFTest=pd.read_csv("/eos/home-z/zijia/Hmumu/tthmumustudies/mini_csv/fold_%d/%s_val.csv"%(fold,fileName))
        #print("start filliing signal and bkg 2nd")
        if i<3:
            dFTrain["target"]=1
            dFTest["target"]=1
            dFTrain["WeightProd"]=dFTrain["Weight"]*dFTrain["Weight_BTagSF85"]
            dFTest["WeightProd"]=dFTest["Weight"]*dFTest["Weight_BTagSF85"]
            dFTrain["WeightFinal"]=dFTrain["WeightProd"]/np.average(dFTrain["WeightProd"])
            dFTest["WeightFinal"]=dFTest["WeightProd"]/np.average(dFTest["WeightProd"])
        else:
            dFTrain["target"]=0
            dFTest["target"]=0
            dFTrain["WeightProd"]=dFTrain["Weight"]*dFTrain["Weight_BTagSF85"]
            dFTest["WeightProd"]=dFTest["Weight"]*dFTest["Weight_BTagSF85"]
            dFTrain["WeightFinal"]=dFTrain["WeightProd"]/sum_bkg_train*sum_signal_train
            dFTest["WeightFinal"]=dFTest["WeightProd"]/sum_bkg_test*sum_signal_test
        #print("end of defining WeightFinal")
        dFTrainList.append(dFTrain)
        dFTestList.append(dFTest)
    totalTrainDF=pd.concat(dFTrainList,ignore_index=True)
    totalTestDF=pd.concat(dFTestList,ignore_index=True)
    #totalTrainDF["splitIndex"]=(totalTrainDF.index%5)
    #print(totalTrainDF)
  
    dTrain[fold]=xgb.DMatrix(totalTrainDF[trainVar],label=totalTrainDF["target"],feature_names=trainVar,weight=totalTrainDF["WeightFinal"]*100)
    dVal[fold]=xgb.DMatrix(totalTestDF[trainVar],label=totalTestDF["target"],feature_names=trainVar,weight=totalTestDF["WeightFinal"]*100)
  
    evallist  = [(dTrain[fold], 'train'), (dVal[fold], 'eval')]
    evals_result = {}
    model=xgb.train(params,dTrain[fold],evals=evallist,evals_result=evals_result,num_boost_round=2000000, early_stopping_rounds=10)
    #model=xgb.train(params,dTrain[fold],evals=evallist,evals_result=evals_result,num_boost_round=2000000, early_stopping_rounds=50)
    model.save_model("%s/%s/fold_%d.model"%(args.outFolder,args.var,fold))
  
    #aucValue = aucValue + max(evals_result['eval']['auc'])
    loglossR = loglossR + min(evals_result['eval']['logloss'])
    #print(max(evals_result['eval']['auc']))
    print(min(evals_result['eval']['logloss']))
  
    fig,ax=plt.subplots()
  
    xgb.plot_importance(model,ax)
    fig.tight_layout()
  
    fig.savefig("%s/%s/VarImportance_fold%d.pdf"%(args.outFolder,args.var,fold))

print(loglossR/4)
