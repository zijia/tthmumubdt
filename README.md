# mini_csv
```
cd for_training/
```

# HPO
```
cdhpo
source zihang_setup.sh
rm -r userdata/*/*Hmumu*
source examples/setup/Hmumu_ttH.sh
hpogrid run Hmumu_ttH
hpogrid report userJobMetadata.json
```

# train
```
source setup.sh
python train_bdt.py -f 1/2/3/4
```

# WIP apply
```
ssh -L 9005:localhost:9005 jiazh@lxslc7.ihep.ac.cn
ssh -L 9005:localhost:9005 zijia@lxplus7.cern.ch
jupyter notebook --no-browser --port 9005
```
